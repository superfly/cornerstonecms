from cornerstone.app import create_app

application = create_app('/app/cornerstone.conf')
