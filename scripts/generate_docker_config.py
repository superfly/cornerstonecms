from argparse import ArgumentParser
from pathlib import Path
from random import choices
from string import Template, ascii_letters, digits, punctuation


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-s', '--site-name', required=True, help='Site name')
    parser.add_argument('-n', '--database-name', required=True, help='Database name')
    parser.add_argument('-u', '--username', required=True, help='Database user')
    parser.add_argument('-p', '--password', required=True, help='Database password')
    parser.add_argument('-r', '--root-password', required=True, help='Database root password')
    parser.add_argument('-d', '--data-dir', required=True, help='Directory to store data in')
    return parser.parse_args()


def generate_file(template_path, filename_path, template_vars):
    shortname = filename_path.name
    print('Generating {}...'.format(shortname), end='')
    with template_path.open() as template_file:
        template = Template(template_file.read())
    with filename_path.open('w') as output_file:
        output_file.write(template.substitute(**template_vars))
    print('done.')


def generate_secret_key():
    return ''.join(choices(ascii_letters + digits + punctuation, k=40))


def main():
    """Build a Docker Compose set up"""
    args = parse_args()
    template_vars = {
        'secretkey': generate_secret_key(),
        'sitename': args.site_name,
        'rootpass': args.root_password,
        'dbhost': 'database',
        'dbname': args.database_name,
        'dbuser': args.username,
        'dbpass': args.password,
        'datadir': str(Path(args.data_dir).resolve())
    }
    base = Path(__file__).resolve().parent.parent
    generate_file(base / 'docker-compose.yaml.example', base / 'docker-compose.yaml', template_vars)
    generate_file(base / 'sphinx.conf.example', base / 'docker' / 'sphinx.conf', template_vars)
    generate_file(base / 'cornerstone.conf.example', base / 'docker' / 'cornerstone.conf', template_vars)


if __name__ == '__main__':
    main()
